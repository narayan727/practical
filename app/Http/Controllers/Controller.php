<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Team;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
    function index(){
        return view("home");
    }

    function createteam(Request $request){
         $request->validate([
            'team'=>'required',
        ]);
        $teamlist = json_decode($request->team);
        $teamdata=[];
        Team::truncate();
        foreach($teamlist as $val){
            $teamdata[]['name']=$val;
        }
        Team::insert($teamdata);
        $playoff=Team::inRandomOrder()->limit(4)->update(['playoff'=>1]);
        $playoff=Team::get();
        return $playoff;
    }

    function qualifier(){
        $qualifier=Team::where("playoff",1)->inRandomOrder()->limit(2)->update(['qualifier'=>1]);
        $qualifier=Team::get();
        return $qualifier;
    }

    function final(){
        $final=Team::where("qualifier",1)->inRandomOrder()->limit(1)->update(['final'=>1]);
        $final=Team::get();
        return $final;
    }
}
