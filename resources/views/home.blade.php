@extends('layouts.app')
@push('css')
<style type="text/css">
    div#loader {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        background: #ffffffa8;
        z-index: 1;
    }
</style>
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Team</div>

                <div class="card-body">
                    <div id="loader" style="display:none;">
                        <div class="spinner-border" role="status">
                          <span class="sr-only"></span>
                        </div>
                    </div>
                    <form id="team-form" action="" method="post">
                        <div class="form-group mb-3">
                            <label for="teamname">Team Name <span id="team-count">1</span></label>
                            <input type="text" id="teamname" class="form-control">
                            <div class="input-err text-danger"></div>

                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-info text-white" id="submit">Next Team</button>
                        </div>
                    </form>
                    <div id="playoff-tab" style="display: none;">
                        <div><label>green:winner</label></div>
                        <div><label>red:loser</label></div>
                        <ul class="list-group"></ul>
                        <div class="text-center mt-3">
                            <button type="submit" class="btn btn-info text-white" id="playoff-tab-btn">Next</button>
                        </div>
                    </div>

                    <div id="qualifier-tab" style="display: none;">
                        <div><label>green:winner</label></div>
                        <div><label>red:loser</label></div>
                        <ul class="list-group"></ul>
                        <div class="text-center mt-3">
                            <button type="submit" class="btn btn-info text-white" id="qualifier-tab-btn">Next</button>
                        </div>
                    </div>
                    <div id="final-tab" style="display: none;">
                        <div><label>green:winner</label></div>
                        <div><label>red:loser</label></div>
                        <ul class="list-group"></ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript" >
$(document).ready(function(e){
    var teamlist = [];
    $("#team-form").on("submit",function(e){
        e.preventDefault();
        var currentteam = $("#teamname").val();
        if(currentteam =='' || $.trim(currentteam)==''){
            $(".input-err").text("Please Enter Team Name");
            return;
        }
        $(".input-err").text("");
        $("#teamname").val("");
        teamlist.push(currentteam)
        $("#team-count").text(teamlist.length+1)
        if(teamlist.length==7){
            $("#submit").text("Get Playoff List");
        }

        if(teamlist.length==8){
            $("#loader").show()
            const formData = new FormData();
            formData.append("team", JSON.stringify(teamlist));
            $.ajax({
                type:"POST",
                url:"{{url('create-team')}}",
                processData: false,
                contentType: false,
                data:formData,
                success:function(data){
                    var playoffhtml="";
                    data.map((value)=>{
                        console.log(value)
                        var classname=value.playoff?"bg-success":"bg-danger";
                        playoffhtml+=`<li class="list-group-item ${classname}">${value.name}</li>`;

                    })
                    $(".card-header").text("Playoff List");
                    $("#playoff-tab ul").html(playoffhtml)
                    $("#team-form,#loader").hide();
                    $("#playoff-tab").show();
                }
            })
        }
    });

    $("#playoff-tab-btn").on("click",function(e){
        $("#loader").show()
        $.ajax({
            type:"GET",
            url:"{{url('qualifier')}}",
            processData: false,
            contentType: false,
            success:function(data){
                var playoffhtml="";
                data.map((value)=>{
                    var classname=value.qualifier?"bg-success":"bg-danger";
                    playoffhtml+=`<li class="list-group-item ${classname}">${value.name}</li>`;

                })
                $(".card-header").text("Qualifier List");

                $("#qualifier-tab ul").html(playoffhtml)
                $("#playoff-tab,#loader").hide();
                $("#qualifier-tab").show();
            }
        })
    })
    $("#qualifier-tab-btn").on("click",function(e){
        $("#loader").show()
        $.ajax({
            type:"GET",
            url:"{{url('final')}}",
            processData: false,
            contentType: false,
            success:function(data){
                var playoffhtml="";
                data.map((value)=>{
                    var classname=value.final?"bg-success":"bg-danger";
                    playoffhtml+=`<li class="list-group-item ${classname}">${value.name}</li>`;

                })
                $(".card-header").text("Final List");
                $("#final-tab ul").html(playoffhtml)
                $("#qualifier-tab,#loader").hide();
                $("#final-tab").show();
            }
        })
    })
})
</script>
@endpush