<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [App\Http\Controllers\Controller::class, 'index']);
Route::post('/create-team', [App\Http\Controllers\Controller::class, 'createteam']);
Route::get('/qualifier', [App\Http\Controllers\Controller::class, 'qualifier']);
Route::get('/final', [App\Http\Controllers\Controller::class, 'final']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
